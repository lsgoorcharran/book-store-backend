var express = require('express')
var router = express.Router()
const mongodb = require('mongodb');
const ObjectID = require('mongodb').ObjectID;


const DB_NAME = 'Book_store';
const BOOK_COLLECTION_NAME = "Books";

const DB_URI = 'mongodb+srv://Leroy_Goorcharran:theboy10@cluster1-6qvfh.mongodb.net/test?retryWrites=true&w=majority';
const MongoClient = mongodb.MongoClient;
const client = new MongoClient(DB_URI, { useNewUrlParser: true, useUnifiedTopology: true });

//GET request
router.get('/', function (req, res) {
  client.connect(function(err, connection){
    const db = connection.db(DB_NAME); // connecting to the book store database
    db.collection(BOOK_COLLECTION_NAME)
      .find({})
      .toArray(function(find_err, records){
        if (find_err)
          return res.status(500).send(find_err);

        return res.send(records);
      });

  });
})

router.post('/', function(req, res) {
  if (!req.body || req.body.length === 0)
    return res.status(400).send({"message": "record is requried"});

  // data validation
  if (!req.body.title || !req.body.author || !req.body.price)
    res.status(400).send({
      message: "Title, author, price, ... are requried"
    });

  // date is in req.body
  client.connect(function(err, connection){
    const db = connection.db(DB_NAME);

    db.collection(BOOK_COLLECTION_NAME)
      .insertOne(req.body, function(insert_error, data){
        if (insert_error)
          return res.status(500).send({message: "Something went wrong"});

        connection.close();

        return res.status(200).send({message: "Record inserted successfully"});
      });

  });

})

router.put('/:id', function(req, res) {

  client.connect(function(err, connection) {
    if (err)
      return res.status(500).send({error: err});

    const db = connection.db(DB_NAME);
    db.collection(BOOK_COLLECTION_NAME)
      .updateOne({_id: ObjectID(req.params.id)},{$set: req.body}, function(update_err, update_data){
         if (update_err)
          return res.status(500).send({error: update_err, message: "Could not update the record"});

        return res.status(200).send({message: "Update was successful!", data: update_data});
      });

  });
})

router.delete('/id', function(req,res) {

  client.connect(function(err, connection) {
    if (err)
      return res.status(500).send({error: err});

    const db = connection.db(DB_NAME);
    db.collection(BOOK_COLLECTION_NAME)
      .deleteOne({_id: ObjectID(req.params.id)},{$set: req.body}, function(deletion_err, delete_data){
         if (deletion_err)
          return res.status(500).send({error: deletion_err, message: "Could not delete the record"});

        return res.status(200).send({message: "Deletion was successful!", data: delete_data});
      });

  });
})

module.exports = router
