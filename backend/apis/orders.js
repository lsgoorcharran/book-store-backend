var express = require('express')
var router = express.Router()
const mongodb = require('mongodb');
const ObjectID = require('mongodb').ObjectID;

const DB_NAME = 'Book_store';
const BOOK_COLLECTION_NAME_TWO = 'Orders';


const DB_URI = 'mongodb://localhost:27017';
const MongoClient = mongodb.MongoClient;
const client = new MongoClient(DB_URI, { useNewUrlParser: true, useUnifiedTopology: true });

router.get('/', function (req, res) {
  client.connect(function(err, connection){
    const db = connection.db(DB_NAME); // connecting to the book store database
    db.collection(BOOK_COLLECTION_NAME_TWO)
      .find({})
      .toArray(function(find_err, records){
        if (find_err)
          return res.status(500).send(find_err);

        return res.send(records);
      });

  });
})

router.post('/', function(req, res) {

  if (!req.body || req.body.length === 0)
    return res.status(400).send({"message": "record is requried"});
  // data validation
  if (!req.body.user_ID || !req.body.book_ID || !req.body.number || !req.body.shipping_address || !req.body.total_price)
    res.status(400).send({
      message: "User_ID, Book_ID, Number,shipping address, total price ... are requried"
//  if (!req.body.book_ID || !req.body.book_ID.lenght > 0)
  //  res.status(400).send({message: "Book IDs are reqired"})
      //figure out to make a error message if there are not enough stock for a book to buy.
    });
  // date is in req.body
  client.connect(function(err, connection){
    const db = connection.db(DB_NAME);
    db.collection(BOOK_COLLECTION_NAME_TWO)
      .insertOne(req.body, function(insert_error, data){
        if (insert_error)
          return res.status(500).send({message: "Something went wrong"});
        connection.close();
        return res.status(200).send({message: "Record inserted successfully"});
      });
  });
})

router.put('/:id', function(req, res) {

    client.connect(function(err, connection) {
      if (err)
        return res.status(500).send({error: err});

      const db = connection.db(DB_NAME);
      db.collection(BOOK_COLLECTION_NAME_TWO)
        .updateOne({_id: ObjectID(req.params.id)},{$set: req.body}, function(update_err, update_data){
           if (update_err)
            return res.status(500).send({error: update_err, message: "Could not update the record"});

          return res.status(200).send({message: "Update was successful!", data: update_data});
      });
  });
})

router.delete('/id', function(req,res) {

    client.connect(function(err, connection) {
      if (err)
        return res.status(500).send({error: err});

      const db = connection.db(DB_NAME);
      db.collection(BOOK_COLLECTION_NAME_TWO)
        .deleteOne({_id: ObjectID(req.params.id)},{$set: req.body}, function(deletion_err, delete_data){
           if (deletion_err)
            return res.status(500).send({error: deletion_err, message: "Could not delete the record"});

          return res.status(200).send({message: "Deletion was successful!", data: delete_data});
        });
    });
})

module.exports = router
