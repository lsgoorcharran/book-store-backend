console.log("Hello World")
const BACKEND_URL = "http://localhost:5050/users"

async function fetchData() {
  const request = new Request(BACKEND_URL, {method: 'GET'})
  const response = await fetch(request)
  const result = await response.json()

  document.getElementById('table').innerHTML = "";

  let htmlCode = '';

  for(var i = 0; i < result.length; i++){
  htmlCode += '<tr>'
  htmlCode += '<td>' + result[i].username + '</td>'
  htmlCode += '<td>' + result[i].password + '</td>'
  htmlCode += '<td>' + result[i].email + '</td>'
  htmlCode += '<td> <button onclick="deleteData()">DELETE</button> </td>'
  htmlCode += '<td> <button onclick="updateData()">UPDATE</button> </td>'
  htmlCode += '</tr>'
  }
document.getElementById('table').innerHTML += htmlCode;

}

async function postData() {
  const data = {username: '', password: '', email: 0}
  data.username = document.getElementById('username').value  //get the ID from the index.html
  data.password = document.getElementById('password').value
  data.email = document.getElementById('email').value

  const request = new Request(BACKEND_URL, {
    method: 'POST',
    headers: {'Content-Type': 'application/json'},
    body: JSON.stringify(data) //turns data into a string
  })

  const response = await fetch(request)
  if(response.status === 400) {
    console.log("There was an error here. 400")
  } else if(response.status === 500) {
    console.log("Server error 500")
  }
  const result = await response.json();
  console.log("Record was inserted successfully!")
  console.log(result)

  fetchData();
}

async function deleteData() {
  const data = {username: '', password: '', email: 0}
  data.username = document.getElementById('username').value  //get the ID from the index.html
  data.password = document.getElementById('password').value
  data.email = document.getElementById('email').value

  const request = new Request(BACKEND_URL, {
    method: 'DELETE',
    headers: {'Content-Type': 'application/json'},
    body: JSON.stringify(data) //turns data into a string
  })

  const response = await fetch(request)

  if(response.status === 400) {
    console.log("There was an error here. 400")
  } else if(response.status === 500) {
    console.log("Server error 500")
  }
  const result = await response.json();
  console.log("Record was deleted successfully!")
  console.log(result)

  fetchData();
}
async function putData() {
  const data = {username: '', password: '', email: 0}
  data.username = document.getElementById('username').value  //get the ID from the index.html
  data.password = document.getElementById('password').value
  data.email = document.getElementById('email').value

  const request = new Request(BACKEND_URL, {
    method: 'PUT',
    headers: {'Content-Type': 'application/json'},
    body: JSON.stringify(data) //turns data into a string
  })

  const response = await fetch(request)
  if(response.status === 400) {
    console.log("There was an error here. 400")
  } else if(response.status === 500) {
    console.log("Server error 500")
  }
  const result = await response.json();
  console.log("Record was updated successfully!")
  console.log(result)

  fetchData();
}
